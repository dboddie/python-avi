#!/usr/bin/env python

"""
fill.py - Shows how to write video frames to an AVI file.

Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from avi import AVI, VideoStream
import Image

def draw_spiral(image, avi):

    x = 199
    y = 199
    l = 1
    d = 1
    
    c = (255, 0, 0)
    
    while l < 400:
    
        image = image.copy()
        image.format = "PNG"
        
        xs = 0
        while xs != l:
            x += d
            image.putpixel((x, y), c)
            xs += 1
        
        c = c[1:3] + c[:1]
        
        ys = 0
        while ys != l:
            y += d
            image.putpixel((x, y), c)
            ys += 1
        
        c = c[1:3] + c[:1]
        
        l += 1
        d = -d
        
        avi.add_frame([image])


avi = AVI()
video = VideoStream(400, 400, 24, 25, "MPNG")

image = Image.new("RGB", (400, 400), 0)
image.format = "PNG"
avi.add_frame([image])

draw_spiral(image, avi)

avi.write("temp.avi", [video])
