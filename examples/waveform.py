#!/usr/bin/env python

"""
waveform.py - Shows how to write audio and video to an AVI file.

Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import math, struct
from avi import AVI, AudioStream, VideoStream
import Image

notes = "C D EF G A B"
colours = {"C": (255, 0, 0), "D": (0, 255, 0), "E": (255, 255, 0),
           "F": (0, 0, 255), "G": (255, 0, 255)}

def tone(octave, note):

    i = notes.index(note)
    return 440 * 2**(((52 + i) + (octave * 12) - 49)/12.0)

def make_waveform(avi, note, sample_rate, duration, frames_per_second, width, height):

    f = tone(0, note)
    fc = tone(0, "C")
    
    samples = sample_rate * duration
    samples_per_frame = sample_rate / frames_per_second
    frames = duration * frames_per_second
    
    colour = colours[note]
    oy = 0.2 * height * ("CDEFG".index(note) + 0.5)
    i = 0
    j = 0
    frame = 0
    sound = ""
    
    while i < samples:
    
        t = float(i)/sample_rate
        a = math.sin(2 * math.pi * f * t)
        sound += struct.pack("<H", 4096 * (1 + a))
        
        i += 1
        j += 1
        
        if j == samples_per_frame:
        
            image = Image.new("RGB", (width, height), 0)
            image.format = "PNG"
            
            x = 0
            while x < width:
                a = math.sin(2 * math.pi * (f/fc) * (float(x)/width + float(frame)/frames))
                y = int(oy + (0.2 * height * a/2))
                image.putpixel((x, y), colour)
                x += 1
            
            avi.add_frame([image, sound])
            
            sound = ""
            j = 0
            frame += 1


avi = AVI()
frames_per_second = 25
width = 400
height = 400

video = VideoStream(width, height, 24, frames_per_second, "MPNG")
audio = AudioStream(1, 11025, 16, frames_per_second)

make_waveform(avi, "C", 11025, 1, frames_per_second, width, height)
make_waveform(avi, "D", 11025, 1, frames_per_second, width, height)
make_waveform(avi, "E", 11025, 1, frames_per_second, width, height)
make_waveform(avi, "F", 11025, 1, frames_per_second, width, height)
make_waveform(avi, "G", 11025, 1, frames_per_second, width, height)

avi.write("temp.avi", [video, audio])
