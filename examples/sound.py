#!/usr/bin/env python

"""
sound.py - Shows how to write sounds to an AVI file.

Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import math, struct
from avi import AVI, AudioStream

def tone(octave, note):

    i = "C D EF G A B".index(note)
    return 440 * 2**(((52 + i) + (octave * 12) - 49)/12.0)

def make_sound(avi, sample_rate, duration):

    f = tone(0, "C")
    samples = sample_rate * duration
    
    i = 0
    data = ""
    while i < samples:
    
        t = float(i)/sample_rate
        a = 4096 * (1 + math.cos(2 * math.pi * f * t))
        data += struct.pack("<H", a)
        i += 1
    
    avi.add_frame([data])

avi = AVI()
audio = AudioStream(1, 11025, 16, 1)

# Make 1 second of a fixed pitch note.
make_sound(avi, 11025, 1)

avi.write("temp.avi", [audio])
