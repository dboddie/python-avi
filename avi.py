"""
avi.py - Read and write Audio Video Interleave (AVI) files.

Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import struct

class AVIError(Exception):
    pass

class AVI:

    """Represents an AVI file for a movie containing a sequence of frames.
    Each frame is itself a sequence of data items representing the video and
    audio content associated with each frame in the movie."""
    
    def __init__(self):
    
        self.frames = []
    
    def write(self, file_name, streams):
    
        """Writes the movie to the file specified by file_name, including the
        content for the sequence of streams described by the AVI object."""
        
        writer = AVIWriter(file_name)
        self._write(writer, streams)
        
        writer.begin_list("movi")
        self.write_frames(writer, streams)
        writer.end_list()
    
    def _write(self, writer, streams):
    
        avih_ = avih(streams)
        
        writer.begin_header()
        writer.begin_list("hdrl")
        writer.write_chunk(avih_)
        
        for stream in streams:
            writer.begin_list("strl")
            writer.write_chunk(stream.strh)
            writer.write_chunk(stream.strf)
            writer.end_list()
        
        # End the hdrl list.
        writer.end_list()
        writer.end_header()
    
    def add_frame(self, pieces):
    
        """Adds a sequence of data items for the next frame in the movie.
        These are added to an internal list which will be written by the
        write_frames method when the movie is written."""
        self.frames.append(pieces)
    
    def write_frames(self, writer, streams):
    
        """Writes the frames using the specified writer where each frame is
        described by the sequence of streams supplied. This method can be
        overridden in subclasses to supply frame data to the writer on demand.
        """
        
        for frame in self.frames:
            writer.write_frame(zip(frame, streams))


class AVIWriter:

    """Handles writing of movies as AVI files."""
    
    def __init__(self, file_name):
    
        """Initialises the AVIWriter object, associating it with the file
        specified by file_name."""
        
        self.f = open(file_name, "wb")
        self.file_name = file_name
        self.addresses = []
        self.placeholders = []
        self.frames = 0
    
    def write_int(self, value):
    
        """Writes an unsigned integer value to the AVI file at the current
        position in the file."""
        self.f.write(struct.pack("<I", value))
    
    def write_str(self, text):
    
        """Writes a string to the AVI file at the current position in the file."""
        self.f.write(text)
    
    def write_placeholder(self, name, value):
    
        """Writes a placeholder value to the AVI file at the current position
        in the file. The name is used to record which type of information needs
        to be filled in later."""
        self.placeholders.append((name, self.f.tell()))
        self.write_int(value)
    
    def begin_header(self):
    
        """Writes the beginning of the AVI header to the file."""
        self.f.write("RIFF")
        self.write_placeholder("length", 0)
        
        self.f.write("AVI ")
    
    def begin_list(self, name):
    
        """Writes the beginning of a list of chunks to the AVI file, recording
        the address of its length field so that it can be filled in later when
        end_list is called."""
        self.f.write("LIST")
        self.addresses.append(self.f.tell())
        self.write_int(0)
        self.f.write(name)
    
    def end_list(self):
    
        """Ends the current list of chunks in the AVI file."""
        self.end_chunk()
    
    def begin_chunk(self, name):
    
        """Writes the beginning of a chunk to the AVI file, recording the
        address of its length field so that it can be filled in later when
        end_chunk is called."""
        self.f.write(name)
        self.addresses.append(self.f.tell())
        self.write_int(0)
    
    def end_chunk(self):
    
        """Ends the current chunk in the AVI file. Fills in its length field
        and ensures that any following content begins on a two-byte boundary."""
        
        address = self.f.tell()
        length_field_address = self.addresses.pop()
        self.f.seek(length_field_address)
        self.write_int(address - length_field_address - 4)
        self.f.seek(address)
        
        # Apparently we have to ensure that each chunk is padded out to a two-
        # byte boundary.
        while self.f.tell() % 2 != 0:
            self.f.write("\x00")
    
    def end_header(self):
    
        """Ends the AVI file header and appends a chunk containing padding to
        ensure that following content begins 2048 bytes into the file."""
        
        offset = self.f.tell() % 0x800
        
        while offset != 0:
        
            remaining = 2048 - offset
            if remaining >= 8:
                self.begin_chunk("JUNK")
                self.f.write("\x00" * (remaining - 8))
                self.end_chunk()
            
            offset = self.f.tell() % 0x800
    
    def write_chunk(self, chunk):
    
        """Writes the contents of the given chunk to the AVI file."""
        chunk.write(self)
    
    def write_frame(self, pairs):
    
        """Writes the content associated with a frame to the AVI file,
        specified as a sequence of pairs, where the first element of each pair
        is the data to be written and the second element is the stream the data
        is associated with."""
        
        self.begin_list("rec ")
        
        i = len(pairs) - 1
        while i >= 0:
        
            data, stream = pairs[i]
            
            if isinstance(stream, AudioStream):
                self.begin_chunk("%02iwb" % i)
                self.f.write(data)
                self.end_chunk()
            
            elif isinstance(stream, VideoStream):
            
                self.begin_chunk("%02idc" % i)
                if hasattr(data, "save"):
                    # Support Image objects as frame data.
                    data.save(self.f, data.format)
                else:
                    self.f.write(data)
                self.end_chunk()
            
            else:
                raise AVIError, "Unknown stream type: %s" % repr(stream)
            
            i -= 1
        
        self.end_list()
        
        self.frames += 1
    
    def close(self):
    
        """Closes the AVI file after filling in any placeholders."""
        
        length = self.f.tell()
        
        for name, address in self.placeholders:
            if name == "length":
                self.write_int(length)
            elif name == "frames":
                self.write_int(self.frames)
        
        self.f.close()


class IncrementalAVI(AVI):

    """Represents an AVI file for a movie containing a sequence of frames.
    Each frame is itself a sequence of data items representing the video and
    audio content associated with each frame in the movie."""
    
    def begin(self, file_name, streams):
    
        """Writes the movie to the file specified by file_name, including the
        content for the sequence of streams described by the AVI object."""
        
        writer = AVIWriter(file_name)
        avih_ = avih(streams)
        
        writer.begin_header()
        writer.begin_list("hdrl")
        writer.write_chunk(avih_)
        
        for stream in streams:
            writer.begin_list("strl")
            writer.write_chunk(stream.strh)
            writer.write_chunk(stream.strf)
            writer.end_list()
        
        # End the hdrl list.
        writer.end_list()
        writer.end_header()
        
        writer.begin_list("movi")
        
        self.streams = streams
        self.writer = writer
    
    def add_frame(self, pieces):
    
        """Adds a sequence of data items for the next frame in the movie.
        These are added to an internal list which will be written by the
        write_frames method when the movie is written."""
        
        self.writer.write_frame(zip(pieces, self.streams))
    
    def end(self):
    
        self.writer.end_list()


class AVIChunk:

    def __init__(self, name):
    
        self.name = name


class avih(AVIChunk):

    def __init__(self, streams):
    
        AVIChunk.__init__(self, "avih")
        
        self.time_per_frame = 0
        self.initial_frame = 0
        self.streams = len(streams)
        self.width = 0
        self.height = 0
        
        # Fill in the details of this chunk from the other chunks.
        for stream in streams:
        
            self.time_per_frame = int(1e6/stream.frames_per_second)
            
            if isinstance(stream, VideoStream):
                self.initial_frame = stream.strh.initial_frame
                self.width = stream.width
                self.height = stream.height
                break
    
    def write(self, writer):
    
        writer.begin_chunk(self.name)
        writer.write_int(self.time_per_frame)
        writer.write_int(0)
        writer.write_int(0)
        writer.write_int(0)
        writer.write_placeholder("frames", 0)
        writer.write_int(self.initial_frame)
        writer.write_int(self.streams)
        writer.write_int(0)
        writer.write_int(self.width)
        writer.write_int(self.height)
        writer.write_int(0)
        writer.write_int(0)
        writer.write_int(0)
        writer.write_int(0)
        writer.end_chunk()


class strh(AVIChunk):

    def __init__(self, stream_type, stream_handler = "\x00\x00\x00\x00"):
    
        AVIChunk.__init__(self, "strh")
        
        self.stream_type = stream_type
        self.stream_handler = stream_handler
        
        self.flags = 0
        self.initial_frame = 0
        self.time_scale = 1
        self.frames_per_second = 50
        self.start_time = 0
    
    def write(self, writer):
    
        writer.begin_chunk(self.name)
        writer.write_str(self.stream_type)
        writer.write_str(self.stream_handler)
        writer.write_int(self.flags)                # flags
        writer.write_int(0)                         # priority and language
        writer.write_int(self.initial_frame)        # initial frame
        writer.write_int(self.time_scale)           # time scale
        writer.write_int(self.frames_per_second)    # frames per second
        writer.write_int(self.start_time)           # starting time
        writer.write_placeholder("frames", 0)
        writer.write_int(0)                         # suggested buffer size
        writer.write_int(0)                         # quality
        writer.write_int(0)                         # sample size (variable)
        writer.write_int(0)                         # frame
        writer.write_int(0)                         # frame
        writer.end_chunk()


class Video_strf(AVIChunk):

    def __init__(self):
    
        AVIChunk.__init__(self, "strf")
        
        self.bit_planes = 1
        self.depth = 24
        self.stream_handler = "MPNG"
    
    def write(self, writer):
    
        writer.begin_chunk(self.name)
        writer.write_int(0)
        writer.write_int(0)
        writer.write_int(0)
        writer.write_int(self.bit_planes | (self.depth << 16))
        writer.write_str(self.stream_handler)
        writer.write_int(0)
        writer.write_int(0)
        writer.write_int(0)
        writer.write_int(0)
        writer.write_int(0)


class Audio_strf(AVIChunk):

    def __init__(self):
    
        AVIChunk.__init__(self, "strf")
        
        self.stream_handler = "MPNG"
        self.format = 1                 # PCM
        self.channels = 1
        self.sample_rate = 31250
        self.bits = 16
    
    def write(self, writer):
    
        writer.begin_chunk(self.name)
        writer.write_int(self.format | (self.channels << 16))
        writer.write_int(self.sample_rate)
        writer.write_int(self.sample_rate)
        writer.write_int(((self.channels * self.bits) >> 3) | (self.bits << 16))
        writer.end_chunk()


class VideoStream:

    def __init__(self, width, height, depth, frames_per_second, stream_handler):
    
        self.width = width
        self.height = height
        self.frames_per_second = frames_per_second
        self.stream_handler = stream_handler
        
        self.strh = strh("vids", stream_handler)
        self.strh.frames_per_second = frames_per_second
        self.strf = Video_strf()
        self.strf.depth = depth
        self.strf.stream_handler = stream_handler


class AudioStream:

    def __init__(self, channels, sample_rate, bits, frames_per_second):
    
        self.channels = channels
        self.sample_rate = sample_rate
        self.bits = bits
        self.frames_per_second = frames_per_second
        
        self.strh = strh("auds")
        self.strf = Audio_strf()
        self.strf.channels = channels
        self.strf.sample_rate = sample_rate
        self.strf.bits = bits
